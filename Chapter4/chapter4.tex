%!TEX root = ../thesis.tex
%*******************************************************************************
%***************************** Fourth Chapter **********************************
%*******************************************************************************
\chapter{Results}

% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter4/Figs/Raster/}{Chapter4/Figs/PDF/}{Chapter4/Figs/}}
\else
    \graphicspath{{Chapter4/Figs/Vector/}{Chapter4/Figs/}}
\fi

\section{Presenting the results of the engineering thesis project}

In this chapter, the results of the running application will be presented. This will be fairly limited since not everything can be seen in the images themselves - like people tracking application shows its functionality only when seeing it running live or on video - but hopefully, the screenshots will be enough to showcase it

\subsection{Starting the central node}

As a main data gathering point of our application, the central node needs to be started up. By default, it should start listening on port 8765 for incoming connections.

After a successful startup, the open ports on the machine show that the server is working

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{ports.eps}
\caption[ports]{List of ports opened by python applications}
\end{center}
\end{figure}

\subsection{Starting the dashboard}

To be able to see the dashboard, some WWW server can be used, like Nginx or just open up the HTML file as a regular file inside the browser - either way, the image presented below should appear on the screen

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{dashboard.eps}
\caption[dashboard]{Dashboard showing no results}
\end{center}
\end{figure}

\subsection{People counting demo}

Initially, this demo and other applications’ demos were meant to be shown in normal conditions, with people sitting or moving in front of the camera.
However, due to the COVID-19 pandemic, the images will be put in front of the camera, so that the results still can be seen being produced, while also there is no unnecessary risk of the virus spread. For face detection, both my face and face from the image will be used.

The people counting application will be tested using this image (source: Unsplash)

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peoplecountingimage.eps}
\caption[peoplecountingimage]{Image used for people counting application with 4 people in it}
\end{center}
\end{figure}

After running the application and pointing it to the image, the following results can be seen

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peoplecountingdemo.eps}
\caption[peoplecountingdemo]{Dashboard presenting the correct results and debug window}
\end{center}
\end{figure}

The result was updated immediately, showing the correct amount of people in the image. Red boxes around the people represent the neural network output

\subsection{People tracking demo}

For this demo, a video shared by Adrian Rosebrock in his “OpenCV People Counter” tutorial on the PyImageSearch website will be used. The camera angle and purpose of this video is identical to my use case, but the implementation of our solutions differ greatly.

In the video, people moving upwards or downwards can be seen, both adults and children. There is even one baby stroller visible.

First up, this is what initially can be seen, before starting the video

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackinginit.eps}
\caption[webapppreview]{Dashboard, video and debug window before starting the test}
\end{center}
\end{figure}

That the dashboard shows 0 detected people and there is neural network preview window on which the camera perspective along with the debug data is present.
Starting off the video, the first person moving downwards appears and is correctly caught by the camera

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackingfirst.eps}
\caption[webapppreview]{First person who appeared in the video}
\end{center}
\end{figure}

As he moves more towards the bottom, the application can correctly identify this movement and increase the counter by one - following the assumption that people moving downwards enter the building.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackingsecond.eps}
\caption[webapppreview]{First person was correctly counted}
\end{center}
\end{figure}

Now, following the movie along, 3 other people move downwards and the counter is now showing “4” as a correct number of people in the building

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackingthird.eps}
\caption[webapppreview]{Additional three people moving the same direction as first one}
\end{center}
\end{figure}

Next up, two people are going upwards on the video. As they enter the image, the neural network correctly recognizes their bounding boxes

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackingfourth.eps}
\caption[webapppreview]{Two people moving opposite direction appear on the video}
\end{center}
\end{figure}

And as they move more towards the top of the frame, the application assumes they are leaving the building, decreasing the total number of people by two.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{peopletrackingfifth.eps}
\caption[webapppreview]{Count of people is correctly decreased}
\end{center}
\end{figure}

For the rest of the video, the same schema can be observed - if the person is moving upwards, the number decreases, and if is moving downwards, the number increases.

\subsection{Face recognition demo}

For this demo, both mine and Mr. Barack Obama’s faces will be used to present the correct face identification process.

Before this demo, the application has been run in learning mode two times - once for my face and once for Barack’s face. The learning mode allowed the application to distinguish our faces and then show the correct label assigned to it.

My face, shown in front of the camera, produces a correct result

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{facerecognitionlukasz.eps}
\caption[webapppreview]{Application correctly indentifying presented face}
\end{center}
\end{figure}

Likewise, when shown the president’s face, the application correctly recognized him

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=1\textwidth]{facerecognitionbarrack.eps}
\caption[webapppreview]{Application correctly indentifying other face}
\end{center}
\end{figure}

As can be also seen on the dashboard in the background, the detection result with the correct label is displayed to the user.

\section{Summary}

Looking at the project overall, from the perspective of actually accomplishing the initial goal, I’m very happy with the result and with the issues I stumbled upon while preparing the applications.

The project was quite simple in terms of domain problem, as these applications existed and are available in many tutorials, but the usage of OAK, and making it all work on such a cutting-edge device is a great success for me.

My plan now, with this thesis completed, is to share it publicly with other OAK users, who may benefit from the work I’ve put into the project, serving as a showcase of what can be accomplished with OAK and how to make things work on it.

\chapter*{Bibliography}\label{sec:bibliography}
\addcontentsline{toc}{chapter}{Bibliography}

\begin{enumerate}
\item A. van den Bossche, R. Dalcé and T. Val, "OpenWiNo: An open hardware and software framework for fast-prototyping in the IoT," 2016 23rd International Conference on Telecommunications (ICT), Thessaloniki, 2016, pp. 1-6, doi: 10.1109/ICT.2016.7500490.
\item Bradski, Gary, and Adrian Kaehler. Learning OpenCV: Computer vision with the OpenCV library. " O'Reilly Media, Inc.", 2008.
\item Howse, Joseph. OpenCV computer vision with python. Packt Publishing Ltd, 2013.
\item Mustaffa, Izadora Binti, and Syawal Fikri Bin Mohd Khairul. "Identification of fruit size and maturity through fruit images using opencv-python and rasberry pi." 2017 International Conference on Robotics, Automation and Sciences (ICORAS). IEEE, 2017.
\end{enumerate}

\begin{thebibliography}{9}
\bibitem{person-detection-retail-0013}
OpenVINO's person-detection-retail-0013 model description,
\\\texttt{https://docs.openvinotoolkit.org/latest/\_models\_intel\_person\_detection\_retail\_0013\_description\_person\_detection\_retail\_0013.html}
\bibitem{pedestrian-detection-adas-0002}
OpenVINO's pedestrian-detection-adas-0002 model description,
\\\texttt{https://docs.openvinotoolkit.org/latest/omz\_models\_intel\_pedestrian\_detection\_adas\_0002\_description\_pedestrian\_detection\_adas\_0002.html}
\bibitem{face-detection-retail-0004}
OpenVINO's face-detection-retail-0004 model description,
\\\texttt{https://docs.openvinotoolkit.org/latest/omz\_models\_intel\_face\_detection\_retail\_0004\_description\_face\_detection\_retail\_0004.html}
\bibitem{wiki-vpu}
Wikipedia information about VPUu,
\\\texttt{https://en.wikipedia.org/wiki/Vision\_processing\_unit}
\bibitem{h5a-camera}
H5A Camera sensor product page,
\\\texttt{https://www.avigilon.com/products/cameras-sensors/h5a}
\bibitem{nuc}
SimplyNUC product page,
\\\texttt{https://simplynuc.com/aidevkit/}
\bibitem{megaai}
MegaAI product page,
\\\texttt{https://www.crowdsupply.com/luxonis/megaai}
\bibitem{oak-intro}
OAK introduction by OpenCV,
\\\texttt{https://opencv.org/introducing-oak-spatial-ai-powered-by-opencv/}
\bibitem{oak-promo}
Spartial AI competition page with promo video I appear in,
\\\texttt{https://opencv.org/opencv-spatial-ai-competition/}
\bibitem{hackaday}
Performance measurements between most common VPU configurations vs MegaAI,
\\\texttt{https://hackaday.io/project/163679-luxonis-depthai/log/169343-outperforming-the-ncs2-by-3x}
\bibitem{bw1093}
BW1093 product page, being the origin and same device as MegaAI and OAK,
\\\texttt{https://shop.luxonis.com/products/bw1093}
\bibitem{rpi-power}
Raspberry Pi power consumption data
\\\texttt{https://www.pidramble.com/wiki/benchmarks/power-consumption}
\bibitem{rti-power}
RTX 2080 Ti power consumption data
\\\texttt{https://pclab.pl/news78675.html}
\bibitem{dgx-power}
DGX Workstation power consumption data
\\\texttt{https://docs.nvidia.com/dgx/dgx-station-user-guide/index.html}
\bibitem{blob-converter}
Online MyriadX model blob converter
\\\texttt{http://luxonis.com:8080/}
\bibitem{docs-conversion}
Tutorial about conversion of OpenVINO models on Luxonis Docs site
\\\texttt{https://docs.luxonis.com/tutorials/converting\_openvino\_model/}
\bibitem{websockets-performance}
Tutorial about conversion of OpenVINO models on Luxonis Docs site
\\\texttt{https://blog.feathersjs.com/da2533f13a77}
\end{thebibliography}


