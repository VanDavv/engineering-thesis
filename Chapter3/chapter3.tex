%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Application overview}

% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter3/Figs/Raster/}{Chapter3/Figs/PDF/}{Chapter3/Figs/}}
\else
    \graphicspath{{Chapter3/Figs/Vector/}{Chapter3/Figs/}}
\fi

\section{Project requirements \& architecture}
Having defined both the hardware architecture and project goal, the next step is to create software architecture and proper modules that will work together to achieve the goal.

After thinking about the overall goal, and to demonstrate different capabilities of the camera setup, the following procedures were selected to automate:

\begin{itemize}
    \item Counting people in front of the camera, which automates the procedure to count peoples in conference rooms or at daily standups to measure space utilization
    \item Tracking people movement direction, allowing to automate the procedure of counting how many people are inside a building / room / alley, as people moving one direction are leaving and going the opposite direction they’re entering
    \item Face recognition for non-secure procedures, so likely in checking e.x. daily schedule, in-office voice messages, in the company sweet shop, etc.
\end{itemize}

My requirements, as a user, would consist of:

\begin{itemize}
    \item Having a single dashboard where all the results acquired by the cameras can be seen
    \item All three procedures work independently, in a plug-and-play manner, meaning if one of the procedures is not working, it should not affect the runtime of the two others
\end{itemize}

Other requirements, not specified above, will come out of best practices and experience gained over the years of creating similar applications.
To name a few of them

\begin{itemize}
    \item Central node architecture - means having a single node that is the core of our system, which holds all the data and properly serves them. The benefit of this approach is the ease of implementation as all client nodes do not need to bother how to store the data and if something will be wrong with data handling, there is a single place to check what’s wrong. One of the obvious downsides of this is a vulnerability to cyberattacks - but that’s not the main goal of this thesis.
    \item Push architecture - where the central node is not accessing any of the client nodes directly, only setting up an API for the clients to either retrieve the data or insert it. This allows the client apps to work independently and connect/disconnect without any disturbance of the central node, which fulfills the second user requirement.
\end{itemize}

Having all of the above info in mind, the proposed architecture looks like this

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.6\textwidth]{architecture.eps}
\caption[architecture]{Visualization of the system architecture}
\end{center}
\end{figure}

Ideally, OAKs will be responsible for both people detection, tracking, and face encoding, as their specification allows to do these task on-device with full speed. However, since this technology is in its early stages, if some task will not be accomplishable on it, the calculations will be moved onto the Raspberry Pi host, whose core task is to take the results produced by the camera and send them to the central node API.

Then API will expose two different sets of endpoints - one for the devices using which it will accept sent data, and the second one for the web client to be able to display the data.

\section{Applications workflow}

All three applications - people counting, people tracking, and face recognition - have similar workflow except results parsing phase. That’s why the description of the simplest one will come first - people counting - and then the differences between the other ones will be shown.

Let’s see the OAK modules and their usability:

\begin{itemize}
    \item Inference engine - mandatory. OAK built-in inference engine will be used to obtain neural network results. The model can be optimized to use all two NCEs, 14 SHAVE cores, and 14 CMX blocks, but most likely only 1 NCE, 4 SHAVE, and 4 CMX will be used, as these are the default values used by OAK and it’s proven to be stable and fast enough to meet our needs.
    \item Object tracker - optional. The built-in object tracker allows one to track objects visible on the screen without any additional load to the host. However, today’s state of it have issues with tracking multiple objects, as it doesn’t “forget” about tracked objects and assigns the same id to the new person that appeared in the image, because it showed in a similar location where the previous person disappeared, and in this case, it should be a new id assigned.
    \item Multi-stage inference - optional. OAK modules are capable of running multiple neural networks at once, even in a multi-stage approach, where, taking face encoding as an example, the face detection could come first, then crop the image to contain only this piece and then feed this cropped frame into the face encoding network - and all of this can be done on the device itself, without any additional load to the host side. However, similarly to the previous module, this one is not yet ready to be used in this thesis, as there’s no Python API to create this inference pipeline, it’s currently only doable using C++ and a way to do so is a subject of change.
\end{itemize}

Taking the above into consideration, only the inference engine will be used for this thesis, and both object tracking and face encoding will be done on the host side using the dlib toolkit.
In the future, maybe even around 2021, the implementation of the workflows may be completely moved to the OAK modules, as the barriers mentioned above will be resolved over time, so then there will be no need to use dlib or any other host-side tool for getting the results.

\subsection{Preparing correct models and configuration}

For OAK inference engine usage, it needs to have provided two files during its initialization:

\begin{itemize}
    \item MyriadX blob, containing the neural network and weights precompiled for MyriadX usage
    \item Blob configuration file, serving as a mapping between OAK device and host side. It defines output shapes, field mappings, precision type, and other network-specific attributes.
\end{itemize}

Thankfully, it’s pretty easy to create the files. Networks that will be used are available on the OpenVINO toolkit and have been even already pre-compiled for OAK usage, along with example configuration files.

However, if there is a need, there is an ability to create them from scratch using the tools created for this purpose - either using Online Blob Converter for MyriadX\cite{blob-converter} or following the conversion tutorial available on the Luxonis Docs site\cite{docs-conversion}

\subsection{Initialization workflow}

For the OAK to be able to begin the inference process, it needs to be initialized properly. Below there is a list of steps that happens during it:
\begin{enumerate}
    \item \textbf{USB connection in a "ready" state}. OAK reports itself as a USB device and is in a state that allows it to be configured
    \item \textbf{Creating a pipeline configuration}. Between OAK and the host, a pipeline is a medium that connects these two parts. Pipeline config, which has to be prepared beforehand, defines all of the runtime options, like camera resolutions, desired output streams, etc.
    \item \textbf{Booting up a “running” OAK}. As soon as the configuration is ready, it is sent to the device. Upon receiving, it disconnects itself from the host, boots up using this config, and attaches again, as a different USB device, now providing only the results requested by the user in config.
    \item \textbf{Consuming the results from the pipeline}. The host-side OAK software can attach to the running device and consume the results from it using the pipeline, which can be used for further processing.
\end{enumerate}

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.8\textwidth]{appworkflow.eps}
\caption[appworkflow]{Initialization steps of the DepthAI pipeline}
\end{center}
\end{figure}

After this step, the device has all of the configurations it needs and feeds the host with the results, whether they’re bounding boxes of detected people or bounding boxes of their faces.
Moving onward, the utilization of this data will be shown in of the applications.

\subsection{People counting application}

The neural network used by this application returns bounding boxes of the people visible on the camera.
Since there's no interest in any other feature than how many people were detected, the normalization step is skipped (as the bounding box coordinates are in 0-1 range and represent the relative distance in the image, e.x. if the image has dimensions of 200x200, and one of the coordinates pair is “(0.1, 0.5)”, this means that the actual point is at (20, 100))

Also, for the stability improvements, instead of displaying the actual count of the bounding boxes, it is stored in an array and take the most often occurring count of the last 100 elements.
The reason for this change is the issue of some of the bounding boxes disappearing, even for a short while, while looking at the same scene with the same people.
By adding this little tweak, the assumption is made that the actual count will be present as the most often occurring value in the counts' array.

\subsection{Sending the data to the central node}

Having the count in place, it needs to be stored in a central node, and the first thing needed for this is to choose the right transport method. There are many options to choose from, but let’s choose from 3 most often used ones:

\begin{itemize}
    \item \textbf{HTTP Requests} are the most common way to communicate with the server. It's an easy and well-known method that is used in most of the web applications known today.
    \item \textbf{Websockets} are also a very common way for client-server communication and are primarily used to provide a bidirectional communication channel between them. They come up with longer setup time, but after a channel is set up, sending the data to the server is done faster than with regular HTTP requests\cite{websockets-performance}.
    \item \textbf{WebRTC} is a relatively new way of web communication, mostly used in real-time applications where time is critical - like in Google Hangouts, Discord, or even Facebook. It’s a peer-to-peer mechanism, which creates a connection between two nodes (as it’s not client-server anymore) using STUN or/and TURN servers. Its downside is complexity, both in the code itself and in the knowledge about the setup itself - compared to the two earlier methods, where all the debug tools are in place, in WebRTC the application needs to deal with correct ICE setup, correct SDP format, setting up tracks for media or data channels for arbitrary data transfer.
\end{itemize}

Websockets were chosen as a communication method because they're pretty easy to set up, the connection will be used constantly while the application is running and the time of delivery is meant to be as low as possible.

Now having the method chosen, the performance of the app comes into consideration. As an example, if the transfer to the server would take only 100ms, the application will be able to run with at max 10 FPS. That’s why, in the implementation process, the multiprocessing library was used, so that the communication with the server is done in the background and the results are shared using thread-safe variables.

\subsection{Handling the data in the central node}

Since the use case of this node is very simple and limits itself to store the data and retrieve it (so it’s like a simplified database), The implementation was kept simple - whole code, handling both the web client and applications, has only 25 lines of code.
The only requirement it has is that the data sent to it must be in JSON format and include the “type” key serving as an application id.

\subsection{Displaying the results in the web client}

As the websockets are supported natively by most browsers, no framework nor packaging mechanism was used, just a simple HTML file with a few lines of javascript code.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.8\textwidth]{webapppreview.eps}
\caption[webapppreview]{Default and empty view of the results dashboard}
\end{center}
\end{figure}

\subsection{People tracking application}

As mentioned earlier, all the steps, including websockets usage and multiprocessing, are common across the applications. What differs them is the models used and processing steps.

In people tracking application, the same model as in people counting is used, but this time the bounding boxes are critical. Since the relevant information is which person went up and which one went down, there's need a way to track people between frames and know their path in the image.

This is achieved by calculating a central point of each bounding box there is and checking for the closest one previous frame point to assign to. If no point is attached or the minimum distance is above the maximum allowed distance, no point is assigned, and instead the centroid is marked as missing. If it’s missing for more frames than the maximum allowed, it’s assumed it's gone and does not consider it for future matchups.

Now, having the tracking mechanism in place, to know the orientation where a person is heading, the maximum and minimum position on the Y-axis is checked (if the relevant information is to know if the person went left or right, the X-axis can be used). If the maximum point has an index before the minimum, the person went up, and if otherwise, the person went down.

And with this information, the total number of people who went up is subtracted from people who went down and receive the information about the total people in the building, assuming the system is started at the point 0, when no one is in the building.


\subsection{Face detection application}

Similar to the previous applications, this one also shares the same initialization and server communication mechanisms.

In this application, however, most of the “heavy lifting” is done by the host side. Since our flow is two-stage inference, with the first stage being the face detection network. In the second stage the application iterates over these results with the face encoding model, which returns 128 values in a vector, representing an encoded face. This flow is not yet fully supported by OAK, and therefore the inference will be split between host and device.

On the device side, the face-detection-retail-0004 network\cite{face-detection-retail-0004} will be used, providing the application with the face frames, ready to be encoded. There are no strong arguments to use this network specifically - accuracy is comparable to other networks available in the OpenVINO Model Zoo models and performance is not relevant (as it will be limited significantly by host-side computations). This network was used simply because it's been battle-tested by the OAK team for the correctness of its results and is included as one of the default networks in the OAK ecosystem.

On the host side, a face feature vector has to be obtained, being a set of 128 discrete values. Vectors of the same face should have a small euclidean distance between themselves, whereas for different faces this distance will be significantly higher.
The dlib framework was chosen using which the distances will be calculated, since, similarly to OpenCV, it has been around for a long time, there are many examples of its usage and one of its features is face encoding. In fact, in dlib this process is split into two phases - landmarks detection and encoding itself - therefore two models need to be loaded for each of these functionalities. Luckily, these are shared freely under the Public Domain license, so they can be used without any restrictions and be up and running in no time.

This application can run in two modes. The first mode is a learning mode, where each of the users stands in front of the camera, and his face encodings are stored along with his name and therefore "learned".
The second mode is an inference mode, where the received encodings are compared to the ones stored in the first stage. If there is a match, meaning the detected encoding distance between one of the stored encodings is less than the predefined threshold, the assigned user name is returned. Otherwise, "unknown" is returned as a person's name.
To further enhance the detection results, the same mechanism as in people counting application is used, storing the detected name in a 100 element array and taking the most often occurring one.

\subsection{Overview}

Having described all of our application components in detail, let's review everything again, as it will help in a better understanding of the results presented in the next chapter.

There will be three applications running simultaneously, one which will detect people count in front of the camera, second that will count people coming in and out, and third that will recognize faces.

All of these applications produce discrete results on the device they're running on and then send them to the central node using websockets.

The central node stores the results, which can be viewed by accessing the dashboard web page it exposes, which fetches the results also using websockets and displays them to the user.


