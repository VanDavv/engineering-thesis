%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************

\chapter{Time \& Hardware Requirements}

\ifpdf
    \graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
    \graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi


\section[Defining time requirements]{Defining time requirements}

Acting in the field of artificial intelligence, the life of the neural network model is divided into two basic phases: training and inference.
The first stage - the learning stage - allows the model to recognize patterns in the provided data. Taking supervised learning as an example, the model is provided with the learning data, containing important factors that affect the value predicted as a result by the model, e.g. the price of an apartment or a photo label. Then, through successive learning epochs, the model tries to guess how the provided data affects the final result - in the case of neural networks, it is done by assigning appropriate weights. This stage will not be covered in this work, as the models that will be used are already pretrained and openly available as part of the both Intel OpenVINO Toolkit Model Zoo and a public domain from Dlib library. These will be the following models:

\begin{itemize}
\item person-detection-retail-0013\cite{person-detection-retail-0013} - network used to detect people and was created for retail scenarios. It’s not so robust as other available networks as it requires the people to be standing parallel to the image plane and the image has to be taken from upright - whereas e.x. pedestrian-detection-adas-0002 is way more flexible in these terms. However, out of all models available in the model zoo from OpenVINO, it performs the best in terms of accuracy, and pose limitations are not an issue for our case since there is full control over the runtime environment of the cameras.
\item face-detection-retail-0004 - network used to detect faces in the image. It uses SqueezeNet light as a backbone and requires the camera to be facing the front of the user.
\end{itemize}

The second stage - the inference stage - is the most notable one. Inference generally means the process of making predictions. In usual data science conditions, this step is not so crucial, since the time available for this stage is not limited (or these limits are not very restrictive), and the highest priority is put onto prediction accuracy. This however shifts when moving to a real-time inference with computer vision. Here, the time for a single detection determines how many frames per second can be analyzed, and therefore the amount of the data collected. For instance, when making license plate detector, the inference time determines how many license plate images can be captured per vehicle driving by; with people tracking, increasing the inference time of people detection will allow the centroid tracker to provide more accurate tracklets assignment, as it operates on the distance between centroids from the previous frame and the current frame, and either creating a new centroid if the distance surpasses threshold or assigning to existing centroid otherwise, therefore the faster the inference can be done, the shorter will be the distance the person travels in the image and the better will be tracking results.

\section[Defining hardware requirements]{Defining hardware requirements}

Moving back to the usual data science field, both training and inference are done on powerful devices like high-performance CPUs or GPU clusters. Mentioned earlier example with people tracking could be done using e.x. NVIDIA DGX-1 workstation with 4x Tesla V100 graphic cards connected with NVLink, on which the inference would be done using NVIDIA DeepStream for the best performance. This approach, despite being applicable to various usages and being very optimized in terms of performance, accuracy, and reliability, comes with many major downsides:
\begin{itemize}
\item The cost of such a setup is tremendous - it raises up to half a million PLN. Of course, it can be lowered by assembling with cheaper devices, using e.x. two RTX 2080 Ti connected with NVLink bridge, however, still this cost would be around 20.000 PLN.
\item Proper infrastructure is required for this setup to work correctly. Starting with proper space where the server will be deposited and moving onward to stable and fast ethernet connection to the cameras, finishing up with proper security measures to assure the video being sent over the network is safe and not leaking outside our network, as it may come with huge financial penalties if detected.
\item Having the machine running comes with high energy consumption, as the GPUs and other peripherals run on full computing power they also eat up a lot of power and makes the bill go considerably higher
\item If such a system is set up once, it’s meant to stay in the exact location for its lifetime or until disassembly. The only way to expand or move the area where the system is operating is by moving the cameras to different places. This, however, is not straightforward to do, since there's no need to maintain a stable camera connection to the server. There is an option to use the Internet, to connect our server with the cameras remotely, but this comes up with latency and instability that is impossible to mitigate.
\end{itemize}

To summarize, the following issues are present with the classical approach: cost, space, connectivity, privacy, energy consumption, and mobility. Of course, there are plenty of use cases, where those issues are not relevant, but ones can be found that are not doable with them - for instance, underwater fish recognition, weed detection, and removal, detections from robots, drones, or even satellites.

\section[Proposed solution]{Proposed solution}

This engineering thesis, despite solving a domain problem of automating the office procedures using computer vision, also aims to address the mentioned above hardware \& software issues, as in my opinion, while creating the project, it’s mutually important “what” to solve as well as “how” to solve it.

First, let’s focus on the software issues and in particular - optimizing inference time. The starting point for most computer vision software is the opencv library, by far the most popular library for computer vision that recently turned 20 years of existence (started in June 2000 by Intel). The code that this library provides, both in Python and C++, it’s pretty well optimized and battle-tested, but knowing the potential of the global developers' community, it must be just a backbone on top of which something more developer-friendly or even more optimized should arise. And as it came out, this thesis was correct.

In 2018, Intel started a new project called OpenVINO Toolkit. Briefly, this library aims to help in the optimization of deep learning models for computer vision usage. It consists of two main components:

\begin{itemize}
  \item Model optimizer - a set of tools that transforms regular model created e.x. in Tensorflow or Caffe, into a new, optimized format, containing one definition file written in XML and one weights file, so it’s using an already trained model to find optimizations that can be made so that the model inference will be faster. In this thesis, there won’t be many references to this module (except the myriad\_compile tool mentioned later), since models distributed by Intel OpenVINO toolkit are already optimized and in the desired format.
  \begin{figure}[htbp!]
  \begin{center}
    \includegraphics[width=1\textwidth]{timecomparison.eps}
    \caption[timecomparison]{Inference times comparison between different hardware setups}
  \end{center}
  \end{figure}
  \item Inference engine - an easy to use API for both C++ and Python that serves as a runtime environment for the deep learning models. It behaves similarly to other known inference environments, like NVIDIA’s TensorRT (which came half a year after OpenVino IE). The thing that was unique for Intel’s runtime was the support for a relatively new family of the computational devices - VPUs.
\end{itemize}

VPU stands for Vision Processing Unit and is an emerging family of processing units, focused on running neural networks for computer vision scenarios. It benefits over GPUs since it does not support rasterization or 3D texture mapping, as the images processed in VPUs are by default two dimensional, which allows VPU to have many optimizations specifically for this, like for bitmap operations or parallel calculations.

Intel currently supports two types of VPU units to be purchased - Myriad X chip and NCS2 USB stick. The former one is designed for hardware manufacturers who want to embed VPU into their products, whereas the latter one is a general-purpose VPU that can be plugged into basically any device like laptop, PC, or Raspberry Pi, and use it’s capabilities in a plug and play manner.

Having purchased the NCS2 stick for the tests, the search began for a device with an embedded camera and Myriad X chip (as stated by the Wikipedia\cite{wiki-vpu}, the chip can be embedded in a way that will directly use the camera interface to fetch the frames).

As a result, the H5A Camera Sensor\cite{h5a-camera} which served as an IP device and was not available to be purchased, SimplyNUC PC\cite{nuc} which is a whole hardware platform that ran Windows and was pretty expensive (3500 PLN for the cheapest version), and MegaAI\cite{megaai} that was communicating over USB3 and had 4K color camera onboard.

The decision was made to test out the last option - the megaAI - and as the research moved onward, it came out that Luxonis company, the creators of this product, we're looking for computer vision specialists to hire. And thanks to this thesis, I’m now part of this company as an Embedded AI developer. Moreover, while this thesis was being written, OpenCV partnered up with Luxonis and released the MegaAI device under their label - OpenCV AI Kit (OAK)\cite{oak-intro}. So, I’ve come back to my starting point (OpenCV with AI) and had a pleasure to be a part of the release of this product (you can see me in the video promoting the OAK release at 2:10\cite{oak-promo}). The OAK camera module is able to do neural inference and have direct access to the camera, so it met my requirements.

Now, having to test out the performance between CPU, NCS2 stick, and OAK, there is a need to choose the host device, serving as a backbone for the project. There were many options available, like the previously mentioned SimplyNUC PC or NVIDIA’s Jetson Nano, but first, their cost of purchase was too high, and second, there was no need for a device to contain neither VPU (SimplyNUC) nor GPU(Jetson). That’s why the final choice was between Dell XPS 9560 (the device the thesis was written on) and Raspberry Pi 3B+. And, taking system independency and power-efficiency into account, the latter one was chosen, having also a low cost of purchase (200 PLN) and high availability.

Now, the only thing left to do is the actual benchmark, to measure out which one is better - CPU, NCS2 stick, or Myriad X chip. And this exact benchmark was made by my CEO Brandon Gilles, who published his results on the hackaday.io blog \cite{hackaday} (DepthAI, as mentioned in the article, was the original name for the hardware that Luxonis produces, with OAK / MegaAI having a codename BW1093\cite{bw1093})
The conclusion was that OAK outperformed NCS2 stick being 3 times faster in terms of inference time and being almost 40 times faster than CPU. Measured times, using MobilenetSSD as the reference network, were respectively 0.64 FPS for CPU, 8.37 FPS for NCS2, and 25.5 FPS for OAK. This gave the solution for software requirements for this project.

Next, the hardware requirements need to be addressed for the project. So let’s head on and pick one by one to describe the solution:

\begin{itemize}
  \item Cost - OAK regular price is 750 PLN, combining it with 200 PLN for RPI 3B+ and some minor peripherals, the total cost of the system is around 1000 PLN
  \item Space - both devices take less space than 100 PLN bill, just as shown in the image below
  \begin{figure}[htbp!]
  \begin{center}
    \includegraphics[width=0.3\textwidth]{depthaisize.eps}
    \caption[depthaisize]{Size of the full DepthAI setup on a 100 PLN bill}
  \end{center}
  \end{figure}
  \item Connectivity - as the camera is onboard the OAK module and the module is connected via USB3 cable, no ethernet connection is required for the system to work. Furthermore, if there would have to be no connections at all, the OAK module (DepthAI BW1093) can be swapped with DepthAI BW1097, which comes with a Raspberry Pi Compute Module Edition - then there's no need for any cables.
  \item Privacy - since the Myriad X chip connects directly to the 4k camera, no data is sent over the network. In this project, the results will be sent over the network, but they will be already aggregated ones (like. people counts as integer or detected person by his name) so there’s no threat of legal penalties
  \item Energy consumption - max power usage of Raspberry Pi 3B+ is approximately 5.1 W\cite{rpi-power} whereas OAK consumes 2.7 W, so there's a total of 7.8 W power usage. For the reference, a single RTX 2080 Ti consumes 250W\cite{rti-power} whereas a DGX-1 workstation - 1500 W\cite{dgx-power}.
  \item Mobility - as the voltage for RPi is 5.1V, therefore using a power bank with usual output port with 2A, this gives us approximately 10W maximum output power. Hence, there is over 2W reserve and our system can be used being attached to power bank safely, therefore allowing it to be fully mobile
\end{itemize}

The above list addresses the hardware requirements specified earlier, and having also the software requirements met, Raspberry Pi 3B+ with the OAK camera module is a way to go with this project.
